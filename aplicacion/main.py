from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from configuracion import config

app = Flask(__name__)

app.config.from_object(config)

db = SQLAlchemy(app)

@app.route('/')
def index ():
    return render_template("base.html")
