from sqlalchemy import Boolean, Column, ForeignKey
from sqlalchemy import DateTime, Integer, String, Text, Float
from sqlalchemy.orm import relationship
from app.db import db

class Articulos(db.model):
    """ARTICULOS DE LA TIENDA"""
    __tablename__ = 'articulos'
    id = Column(Integer, primary_key=True)
    nombre = Column(String(100),nullable=False)
    precio =  Column(Float, default=0)
    descripcion = Column(String(255))
    image = Column(String(255))
    stock = Column(Integer, default=0)
    CategoriaId = Column(Integer, ForeignKey('categorias.id'),nullable=False)
    categoria = relationship("Categorias", backref="Articulos")



    def __repr__(self):
        return (u'<{self.__class__.__name__}:{self.id}>'.format(self=self))